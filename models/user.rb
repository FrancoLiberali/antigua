require_relative 'active_account.rb'

class User
  include ActiveModel::Validations

  attr_accessor :id, :name, :email, :crypted_password, :job_offers,
                :account_state, :account_state_id, :updated_on, :created_on

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :name, :crypted_password, presence: true
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX,
                                              message: 'invalid email' }

  def initialize(data = {})
    @id = data[:id]
    @name = data[:name]
    @email = data[:email]
    @crypted_password = User.get_crypted_password(data)
    @job_offers = data[:job_offers]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @account_state = data[:account_state]
    @account_state_id = data[:account_state_id]
  end

  def self.get_crypted_password(data)
    if data[:password].nil?
      data[:crypted_password]
    else
      Crypto.encrypt(data[:password])
    end
  end

  def has_password?(password)
    @account_state = ActiveAccount.new if @account_state.nil?
    @account_state = @account_state.login(Crypto.decrypt(crypted_password) == password)
    @account_state.loged_in
  end

  def blocked?
    @account_state.blocked?
  end

  def remaining_failed_attempts
    @account_state.remaining_failed_attempts
  end
end
