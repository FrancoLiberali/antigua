require 'date'

class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title, :location,
                :description, :is_active, :updated_on,
                :created_on, :applications_amount, :validity_date, :tags

  validates :title, presence: { message: ' is mandatory' }
  validate :valid_validity_date_format
  validate :valid_validity_date_year

  WITHOUT_APPLICATIONS = 0
  VALID_DATE_FORMAT = '%Y-%m-%d'.freeze

  # rubocop:disable Metrics/AbcSize
  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active] || false
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    @applications_amount = data[:applications_amount] || WITHOUT_APPLICATIONS
    @validity_date = data[:validity_date]
    @tags = data[:tags]
  end
  # rubocop:enable Metrics/AbcSize

  def self.new_from(other)
    JobOffer.new(title: other.title,
                 location: other.location,
                 description: other.description,
                 user_id: other.user_id,
                 validity_date: other.validity_date,
                 tags: other.tags)
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  def apply
    self.applications_amount += 1
  end

  private

  def valid_validity_date_format
    Date.strptime(validity_date.to_s, VALID_DATE_FORMAT)
  rescue ArgumentError
    unless validity_date == '' || validity_date.nil?
      errors.add(:base, 'Valid until date is invalid')
    end
  end

  def valid_validity_date_year
    errors.add(:base, 'Valid until date is invalid') if validity_date.to_s.split('-')[0] == '0000'
  end
end
