require 'date'

class BlockedAccount
  include ActiveModel::Validations

  attr_accessor :id, :blocked_on, :loged_in, :updated_on, :created_on

  NO_REMAINING_FAILED_ATTEMPTS = 0

  def initialize(data = {})
    @id = data[:id]
    @blocked_on = data[:blocked_on] || DateTime.now
    @loged_in = false
    @created_on = data[:created_on]
    @updated_on = data[:updated_on]
  end

  def login(correct_password)
    return self if DateTime.now < @blocked_on + Padrino.config.blocking_account_time

    ActiveAccount.new.login(correct_password)
  end

  def blocked?
    true
  end

  def remaining_failed_attempts
    NO_REMAINING_FAILED_ATTEMPTS
  end
end
