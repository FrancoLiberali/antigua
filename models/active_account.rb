require 'date'

class ActiveAccount
  include ActiveModel::Validations

  attr_accessor :id, :remaining_failed_attempts,
                :loged_in, :updated_on, :created_on

  MAX_REMAINING_FAILED_ATTEMPTS = 3
  NO_REMAINING_FAILED_ATTEMPTS = 0

  validates :remaining_failed_attempts, numericality:
    { only_integer: true,
      greater_than_or_equal_to: 0,
      less_than_or_equal_to: MAX_REMAINING_FAILED_ATTEMPTS }

  def initialize(data = {})
    @id = data[:id]
    @created_on = data[:created_on]
    @updated_on = data[:updated_on]
    @remaining_failed_attempts = data[:remaining_failed_attempts] || MAX_REMAINING_FAILED_ATTEMPTS
    @loged_in = data[:loged_in] || false
  end

  def login(correct_password)
    if correct_password
      @loged_in = true
      @remaining_failed_attempts = MAX_REMAINING_FAILED_ATTEMPTS
    else
      @loged_in = false
      @remaining_failed_attempts -= 1
      return BlockedAccount.new if
        @remaining_failed_attempts == NO_REMAINING_FAILED_ATTEMPTS
    end
    self
  end

  def blocked?
    false
  end
end
