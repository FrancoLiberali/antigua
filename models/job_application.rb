require 'uri'

class JobApplication
  include ActiveModel::Validations

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  VALID_LINK_REGEX = URI.regexp(%w[http https])

  attr_accessor :applicant_email, :job_offer, :cv_link

  validates :applicant_email, presence: true, format: { with: VALID_EMAIL_REGEX,
                                                        message: 'is invalid' }
  validates :cv_link, allow_blank: true, format: { with: VALID_LINK_REGEX,
                                                   message: 'is invalid' }

  def self.create_for(email, offer, cv_link = nil)
    app = JobApplication.new
    app.applicant_email = email
    app.job_offer = offer
    app.cv_link = cv_link
    app
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
    JobVacancy::App.deliver(:application, :applicant_info_email, self)
    job_offer.apply
  end
end
