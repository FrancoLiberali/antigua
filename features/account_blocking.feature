Feature: Account blocking
  In order to increase my account security
  As a user
  I want my account to get blocked after three failed attempts to log in

  Background:
    Given I have an account in Job Vacancy

  Scenario: wrong email log in message
    When I fail to log in with wrong email
    Then I should see "Invalid credentials"

  Scenario: failling attempt to log in messages
    When I fail to log in with wrong password
    Then I should see "Invalid credentials, you have 2 more failed log in attempts before your account get blocked for 10 seconds"

  Scenario: less than three failling attempts to log in not block
    Given I had 2 failed attempts to log in
    When I try to log in with my correct username and password
    Then I should see "Welcome!"

  Scenario: three failling attempts to log in
    Given I had 3 failed attempts to log in
    When I try to log in with my correct username and password
    Then I should see "Your account has been blocked for 10 seconds"

  Scenario: waiting for my account to get unblocked
    Given I had 3 failed attempts to log in
    And I wait 10 seconds
    When I try to log in with my correct username and password
    Then I should see "Welcome!"
