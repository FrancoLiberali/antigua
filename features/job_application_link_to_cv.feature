Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer, including a link to my CV

  Background:
    Given only a "Web Programmer" offer exists in the offers list

  Scenario: Apply to job offer with valid CV link
    Given I access the offers list page
    When I apply with email "applicant@test.com" and CV link "https://linkedin.com/name"
    Then I should receive a mail with offerer info

  Scenario: Apply to job offer with invalid CV link
    Given I access the offers list page
    When I apply with email "applicant@test.com" and CV link "linkedin.com/name"
    Then I should see "Cv link is invalid"
