Feature: Job Application
  As a job offerer
  I want to receive information about my applicants

  Scenario: Someone applies to my offer
    Given only a "Web Programmer" offer exists in the offers list
    When an applicant applies
    Then I should receive an email with applicants info
