Feature: Copy job offers
  In order make easier the job offer creating process
  As a job offerer
  I want to copy existing offers

  Background:
    Given I am logged in as job offerer

  Scenario: Copy a not active offer
    Given I have "Web Programmer" offer in My Offers
    And I access my offers page
    When I copy a offer
    Then I should see "Offer copied"
    And I should see "Web Programmer" 2 times in My Offers
    And I should see "0 applications" 2 times in My Offers
    And I should see "Activate" 2 times in My Offers

  Scenario: Copy an active offer without applications
    Given only a "Web Programmer" offer exists in the offers list
    And I access my offers page
    When I copy a offer
    Then I should see "Offer copied"
    And I should see "Web Programmer" 2 times in My Offers
    And I should see "a nice job" 2 times in My Offers
    And I should see "a nice place" 2 times in My Offers
    And I should see "2019-10-16" 2 times in My Offers
    And I should see "0 applications" 2 times in My Offers
    And I should see "Activate" in My Offers

  Scenario: Copy an active offer with applications
    Given only a "Web Programmer" offer exists in the offers list
    And 2 applicants apply to my offer
    And I access my offers page
    When I copy a offer
    Then I should see "Offer copied"
    And I should see "Web Programmer" 2 times in My Offers
    And I should see "a nice job" 2 times in My Offers
    And I should see "a nice place" 2 times in My Offers
    And I should see "2019-10-16" 2 times in My Offers
    And I should see "2 applications" in My Offers
    And I should see "0 applications" in My Offers
    And I should see "Activate" in My Offers

  Scenario: Copying an offer takes me to the edit interface
    Given only a "Web Programmer" offer exists in the offers list
    And I access my offers page
    When I copy a offer
    Then I should see "Offer copied"
    And I should see "Edit Job Offer"
