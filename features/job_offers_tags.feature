Feature: Job Offers tags

  Background:
    Given I am logged in as job offerer

  Scenario: add tags
    Given I access the new offer page
    And I fill the title with "Programmer vacancy"
    And fill tags with "java, developer"
    When I confirm the new offer
    And activate my offer
    Then I should see "Tags" in My Offers
    And I should see "java" in My Offers
    And I should see "developer" in My Offers
    Then I should see "Tags" in Offers list
    And I should see "java" in Offers list
    And I should see "developer" in Offers list
