Feature: Registration

  Background:
    Given I access the home page


  Scenario: valid registration
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!AntiguaJ_2019"
    And I fill the confirmation password with "!AntiguaJ_2019"
    When I confirm the registrarion
    Then I should see "User created"

  Scenario: short password
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!A1a"
    And I fill the confirmation password with "!A1a"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."

  Scenario: long password
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!A1a123456789123456789123456"
    And I fill the confirmation password with "!A1a123456789123456789123456"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."

  Scenario: password without uppercase
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!a1a123456"
    And I fill the confirmation password with "!a1a123456"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."

  Scenario: password without lowercase
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!A1A123456"
    And I fill the confirmation password with "!A1A123456"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."

  Scenario: password without symbol
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "1a1a123456"
    And I fill the confirmation password with "1a1a123456"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."

  Scenario: password without letter
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!123123456"
    And I fill the confirmation password with "!123123456"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."

  Scenario: password without number
    Given I access to registration
    And I fill the name with "Test"
    And I fill the e-mail with "test@test.com"
    And I fill the password with "!aAasdfghj"
    And I fill the confirmation password with "!aAasdfghj"
    When I confirm the registrarion
    Then I should see "The password must be 8-20 characters long, contain at least one number, one symbol and have a mixture of uppercase and lowercase letters."
