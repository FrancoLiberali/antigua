Feature: Job Offers validity date
  As a job offerer
  I want to set a validity date for my offers

  Background:
    Given I am logged in as job offerer

  Scenario: Create new offer with valid validity date
    Given I access the new offer page
    And I fill the title with "Test"
    When I fill the validity date with "2019-12-20"
    And confirm the new offer
    Then I should see "Offer created"
    And I should see "2019-12-20" in My Offers

  Scenario: Create new offer with invalid validity date
    Given I access the new offer page
    And I fill the title with "Test"
    When I fill the validity date with "add/w/2018"
    And confirm the new offer
    Then I should see "Valid until date is invalid"

  Scenario: Edit offer with valid validity date
    Given I have "Programmer vacancy" offer in My Offers
    And I edit it
    And I set validity date to "2019-12-20"
    And I save the modification
    Then I should see "Offer updated"
    And I should see "2019-12-20" in My Offers

  Scenario: Edit offer with invalid validity date
    Given I have "Programmer vacancy" offer in My Offers
    And I edit it
    And I set validity date to "not/a/date"
    And I save the modification
    Then I should see "Valid until date is invalid"
