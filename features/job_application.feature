Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
  	Given only a "Web Programmer" offer exists in the offers list

  Scenario: Apply to job offer
    Given I access the offers list page
    When I apply with email "applicant@test.com"
    Then I should receive a mail with offerer info

  Scenario: Apply to job offer with valid email
    Given I access the offers list page
    When I apply with email "mi_mail@test.com"
    Then I should receive a mail with offerer info

  Scenario: Apply to job offer with invalid email
    Given I access the offers list page
    When I apply with email "asd1234"
    Then I should see "Applicant email is invalid"
