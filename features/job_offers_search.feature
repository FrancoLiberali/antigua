Feature: Job Offers search
  In order to get a job
  As a candidate
  I want to search an offer

  Background:
    Given "Java dev", location "BSAS" and description "full time" active offer exists
    And "Ruby developer", location "BA" and description "part time" active offer exists
    And "Tester", location "BsAs" and description "full time" active offer exists
    And "Developer", location "BsAs" and description "full time" offer exists

  Scenario: search on more than one attribute
    Given I access the offers list page
    When I search 'dev'
    Then I should see "Java dev"
    And I should see "Ruby developer"
    And I should not see "Tester"
    And I should not see "Developer"

  Scenario: search in case INsensitive
    Given I access the offers list page
    When I search 'BsAs'
    Then I should see "Java dev"
    And I should see "Tester"
    And I should not see "Ruby developer"
    And I should not see "Developer"
