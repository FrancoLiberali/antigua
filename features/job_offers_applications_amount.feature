Feature: Job Offers Applications amount
  In order to hava more information about my offers
  As a job offerer
  I want to know the applications amount of my offers

  Background:
    Given I am logged in as job offerer

  Scenario: Create new offer have no offers
    Given only a "Web Programmer" offer exists in the offers list
    Then I should see "0 applications" in My Offers

  Scenario: Offer that recive applications shows the amount in my offers
    Given only a "Web Programmer" offer exists in the offers list
    And 7 applicants apply to my offer
    Then I should see "7 applications" in My Offers
