When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

Given('I access my offers page') do
  visit '/job_offers/my'
end

When('I copy a offer') do
  click_button('Copy')
end

Then('I should see {string} {int} times in My Offers') do |content, amount|
  visit '/job_offers/my'
  page.should have_content(content, count: amount)
end

When(/^I fill the title with "(.*?)"$/) do |offer_title|
  fill_in('job_offer[title]', with: offer_title)
end

When(/^I fill the validity date with "(.*?)"$/) do |offer_validity_date|
  fill_in('job_offer[validity_date]', with: offer_validity_date)
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then(/^I should see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete')
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', with: new_title)
end

Given(/^I set validity date to "(.*?)"$/) do |new_date|
  fill_in('job_offer[validity_date]', with: new_date)
end

Given(/^I save the modification$/) do
  click_button('Save')
end

When('I search {string}') do |key_word|
  fill_in('q', with: key_word)
  click_button('search')
end

Given('fill tags with {string}') do |tags|
  fill_in('job_offer[tags]', with: tags)
end

When('I confirm the new offer') do
  click_button('Create')
end

Then(/^I should see "(.*?)" in Offers list$/) do |content|
  visit '/job_offers/latest'
  page.should have_content(content)
end

When('activate my offer') do
  click_button('Activate')
end
