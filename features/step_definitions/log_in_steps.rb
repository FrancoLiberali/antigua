require 'cucumber/rspec/doubles'

Given('I have an account in Job Vacancy') do
  @email = 'offerer@test.com'
  @password = 'Passw0rd!'
end

When('I fail to log in with wrong email') do
  visit '/login'
  fill_in('user[email]', with: 'invalid')
  fill_in('user[password]', with: @password)
  click_button('Login')
end

When('I fail to log in with wrong password') do
  visit '/login'
  fill_in('user[email]', with: @email)
  fill_in('user[password]', with: 'invalid')
  click_button('Login')
end

Given('I had {int} failed attempts to log in') do |amount|
  @now = DateTime.now
  DateTime.stub(:now).and_return(@now)
  (1..amount).each do |_i|
    step 'I fail to log in with wrong password'
  end
end

Given('I wait {int} seconds') do |seconds|
  DateTime.stub(:now).and_return(@now + seconds)
end

When('I try to log in with my correct username and password') do
  visit '/login'
  fill_in('user[email]', with: @email)
  fill_in('user[password]', with: @password)
  click_button('Login')
end
