Given(/^only a "(.*?)" offer exists in the offers list$/) do |job_title|
  @job_offer = JobOffer.new
  @job_offer.owner = UserRepository.new.first
  @job_offer.title = job_title
  @job_offer.location = 'a nice place'
  @job_offer.description = 'a nice job'
  @job_offer.validity_date = Date.new(2019, 10, 16)
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Given('{string}, location {string} and description {string} offer exists') do |title, location, description|
  @job_offer = JobOffer.new
  @job_offer.owner = UserRepository.new.first
  @job_offer.title = title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.is_active = false

  JobOfferRepository.new.save @job_offer
end

Given('{string}, location {string} and description {string} active offer exists') do |title, location, description|
  @job_offer = JobOffer.new
  @job_offer.owner = UserRepository.new.first
  @job_offer.title = title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Given(/^I access the offers list page$/) do
  visit '/job_offers/latest'
end

Given('{int} applicants apply to my offer') do |amount|
  (1..amount).each do |_i|
    step 'I access the offers list page'
    step 'I apply'
  end
end

When(/^I apply$/) do
  click_link 'Apply'
  fill_in('job_application[applicant_email]', with: 'applicant@test.com')
  fill_in('job_application[cv_link]', with: 'https://www.linkedin.com/Applicant')
  click_button('Apply')
end

When(/^I apply with email "([^"]*)"$/) do |string|
  click_link 'Apply'
  fill_in('job_application[applicant_email]', with: string)
  click_button('Apply')
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Given('I access the home page') do
  visit '/'
end

Given('I access to registration') do
  visit '/register'
end

Given('I fill the name with {string}') do |user_name|
  fill_in('user[name]', with: user_name)
end

Given('I fill the e-mail with {string}') do |email|
  fill_in('user[email]', with: email)
end

Given('I fill the password with {string}') do |password|
  fill_in('user[password]', with: password)
end

Given('I fill the confirmation password with {string}') do |confirm_password|
  fill_in('user[password_confirmation]', with: confirm_password)
end

When('I confirm the registrarion') do
  click_button('Create')
end

When('an applicant applies') do
  step 'I access the offers list page'
  step 'I apply'
end

Then('I should receive an email with applicants info') do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/offerer@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?('applicant@test.com').should be true
  content.include?('https://www.linkedin.com/Applicant').should be true
end

When('I apply with email {string} and CV link {string}') do |string, string2|
  click_link 'Apply'
  fill_in('job_application[applicant_email]', with: string)
  fill_in('job_application[cv_link]', with: string2)
  click_button('Apply')
end
