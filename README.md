Job Vacancy Application
=======================

[![build status](https://gitlab.com/fiuba-memo2/tp1/antigua/badges/master/build.svg)](https://gitlab.com/fiuba-memo2/tp1/antigua/commits/master)

## PostgreSQL setup

Follow these steps to initialize the PostgreSQL databases:

1. Install PostgreSQL if needed. On Ubuntu you can do this by running:
`sudo apt-get install -y postgresql-9.5 postgresql-contrib postgresql-server-dev-9.5`
1. Create development and test databases by running:
`sudo -u postgres psql --dbname=postgres -f ./create_dev_and_test_dbs.sql`

## Padrino application setup

1. Run **_bundle install --without staging production_**, to install all application dependencies
1. Run **_bundle exec rake_**, to run all tests and ensure everything is properly setup
1. Run **_RACK_ENV=development bundle exec rake db:migrate db:seed_**, to setup the development database
1. Run **_bundle exec padrino start -h 0.0.0.0_**, to start the application

For authenticating as an offerer you can use the credentials offerer@test.com / Passw0rd!

## Some conventions to work on it:

* Follow existing coding conventions
* Use feature branch
* Add descriptive commits messages in English to every commit
* Write code and comments in English
* Use REST routes

## Work environments

GitLab project repository: https://gitlab.com/fiuba-memo2/tp1/antigua
Backlog: https://gitlab.com/fiuba-memo2/tp1/antigua/-/boards

Branch "master" deploys to production app
Link to production app: https://jobvacancy-antigua-production.herokuapp.com/

Branch "develop" deploys to staging app
Link to staging app: https://jobvacancy-antigua-staging.herokuapp.com/

## Workflow

When decided to work on a user story, create a new branch. Complete the story applying BDD/TDD.
Once done, create a merge request from said branch to develop. After code review form the other team members, merge.
Send mail to PO informing about the progress. After they have checked and approved it, merge to master to go to production.