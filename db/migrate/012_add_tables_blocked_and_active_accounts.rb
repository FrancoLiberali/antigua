Sequel.migration do
  up do
    create_table(:blocked_accounts) do
      primary_key :id
      DateTime :blocked_on
      Date :created_on
      Date :updated_on
      foreign_key :user_id
    end

    create_table(:active_accounts) do
      primary_key :id
      Date :created_on
      Date :updated_on
      Integer :remaining_failed_attempts
      foreign_key :user_id
    end
  end

  down do
    drop_table(:blocked_accounts)
    drop_table(:active_accounts)
  end
end
