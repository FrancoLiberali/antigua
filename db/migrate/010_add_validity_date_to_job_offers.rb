Sequel.migration do
  up do
    add_column :job_offers, :validity_date, Date
  end

  down do
    drop_column :job_offers, :validity_date
  end
end
