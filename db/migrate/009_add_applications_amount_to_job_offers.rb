Sequel.migration do
  up do
    add_column :job_offers, :applications_amount, Integer, default: 0
  end

  down do
    drop_column :job_offers, :applications_amount
  end
end
