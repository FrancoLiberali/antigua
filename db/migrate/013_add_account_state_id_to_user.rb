Sequel.migration do
  up do
    add_column :users, :account_state_id, Integer, nullable: false
  end

  down do
    drop_column :users, :account_state_id
  end
end
