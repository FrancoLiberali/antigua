require 'spec_helper'

describe ActiveAccount do
  subject(:active_account) { described_class.new({}) }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:remaining_failed_attempts) }
    it { is_expected.to respond_to(:loged_in) }
    it { is_expected.to respond_to(:updated_on) }
    it { is_expected.to respond_to(:created_on) }
  end

  describe 'remaining failed attempts' do
    let(:password) { 'password' }

    it 'should be 3 remaining failed attempts for a new user' do
      user = described_class.new(password: password,
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe')
      expect(user.remaining_failed_attempts).to be(3)
    end

    it 'should be integer' do
      user = described_class.new(password: password,
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe',
                                 remaining_failed_attempts: 1.5)
      expect(user.valid?).to be(false)
    end

    it 'should be positive' do
      user = described_class.new(password: password,
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe',
                                 remaining_failed_attempts: -1)
      expect(user.valid?).to be(false)
    end

    it 'should be less or equal to 3' do
      user = described_class.new(password: password,
                                 email: 'john.doe@someplace.com',
                                 name: 'john doe',
                                 remaining_failed_attempts: 4)
      expect(user.valid?).to be(false)
    end
  end

  describe 'login' do
    it 'should return itself with one less remaining_failed_attempts
  when no password match' do
      expect(active_account.login(false)).to eq active_account
      expect(active_account.loged_in).to eq false
      expect(active_account.remaining_failed_attempts).to eq 2
    end

    it 'should return itself with the remaining_failed_attempts
  when password match' do
      expect(active_account.login(true)).to eq active_account
      expect(active_account.loged_in).to eq true
      expect(active_account.remaining_failed_attempts).to eq 3
    end

    it 'should return itself with the remaining_failed_attempts
    when password match' do
      active_account.remaining_failed_attempts = 1
      expect(active_account.login(false).class).to eq BlockedAccount
      expect(active_account.login(false).loged_in).to eq false
    end

    it 'should reset the remaining failed attempts when password do match' do
      active_account.remaining_failed_attempts = 1
      expect(active_account.login(true)).to eq active_account
      expect(active_account.loged_in).to eq true
      expect(active_account.remaining_failed_attempts).to eq 3
    end
  end

  it 'should not be blocked' do
    expect(active_account.blocked?).to eq false
  end
end
