require 'spec_helper'

describe JobApplication do
  describe 'model' do
    it { is_expected.to respond_to(:applicant_email) }
    it { is_expected.to respond_to(:job_offer) }
    it { is_expected.to respond_to(:cv_link) }
  end

  describe 'create_for' do
    it 'should set applicant_email' do
      email = 'applicant@test.com'
      ja = described_class.create_for(email, JobOffer.new)
      expect(ja.applicant_email).to eq(email)
    end

    it 'should set job_offer' do
      offer = JobOffer.new
      ja = described_class.create_for('applicant@test.com', offer)
      expect(ja.job_offer).to eq(offer)
    end

    it 'should set cv link' do
      cv_link = 'https://www.linkedin.com/Applicant'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.cv_link).to eq(cv_link)
    end
  end

  describe 'valid?' do
    it 'should be valid with a valid cv link' do
      cv_link = 'https://www.linkedin.com/Applicant'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq true
    end

    it 'should be valid with nil cv link' do
      cv_link = nil
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq true
    end

    it 'should be valid with no cv link' do
      ja = described_class.create_for('applicant@test.com', JobOffer.new)
      expect(ja.valid?).to eq true
    end

    it 'should be invalid with an invalid cv link I' do
      cv_link = 'linkedin.com'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq false
    end

    it 'should be invalid with an invalid cv link II' do
      cv_link = 'htp://www.linkedin.com/Applicant'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq false
    end

    it 'should be invalid with an invalid cv link III' do
      cv_link = 'www.linkedin.com/Applicant'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq false
    end

    it 'should be invalid with an invalid cv link IV' do
      cv_link = 'httpswww.linkedin.com/Applicant'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq false
    end

    it 'should be invalid with an invalid cv link V' do
      cv_link = 'linkedin.com/Applicant'
      ja = described_class.create_for('applicant@test.com', JobOffer.new, cv_link)
      expect(ja.valid?).to eq false
    end
  end

  describe 'process' do
    it 'should deliver contact info notification' do
      ja = described_class.create_for('applicant@test.com', JobOffer.new)
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :contact_info_email, ja)
      expect(JobVacancy::App).to receive(:deliver).with(:application, :applicant_info_email, ja)
      ja.process
    end

    it 'should apply to the job offer' do
      ja = described_class.create_for('applicant@test.com', job_offer = JobOffer.new)
      expect(JobVacancy::App).to receive(:deliver).with(:notification, :contact_info_email, ja)
      expect(JobVacancy::App).to receive(:deliver).with(:application, :applicant_info_email, ja)
      ja.process
      expect(job_offer.applications_amount).to eq(1)
    end
  end
end
