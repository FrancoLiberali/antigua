require 'spec_helper'
require 'date'

describe BlockedAccount do
  subject(:blocked_account) { described_class.new(blocked_on: DateTime.now) }

  let(:now) { blocked_account.blocked_on }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:blocked_on) }
    it { is_expected.to respond_to(:loged_in) }
  end

  describe 'login' do
    let(:blocking_account_time) { Padrino.config.blocking_account_time }

    it 'should return itself when no password match' do
      expect(blocked_account.login(false)).to eq blocked_account
      expect(blocked_account.loged_in).to eq false
    end

    it 'should return itself when no blocked time passed' do
      allow(DateTime).to receive(:now).and_return(now)
      expect(blocked_account.login(true)).to eq blocked_account
      expect(blocked_account.loged_in).to eq false
    end

    it 'should return an ActiveAccount loged_in when the blocked
    time passed and password is correct' do
      allow(DateTime).to receive(:now).and_return(now + blocking_account_time + 1)
      expect(blocked_account.login(true).class).to eq ActiveAccount
      expect(blocked_account.login(true).loged_in).to eq true
    end

    it 'should return an ActiveAccount not loged_in when the blocked
    time passed but password in incorrect' do
      allow(DateTime).to receive(:now).and_return(now + blocking_account_time + 1)
      expect(blocked_account.login(false).class).to eq ActiveAccount
      expect(blocked_account.login(false).loged_in).to eq false
      expect(blocked_account.login(false).remaining_failed_attempts).to eq 2
    end
  end

  it 'should be blocked' do
    expect(blocked_account.blocked?).to eq true
  end

  it 'should have 0 remaining_failed_attempts' do
    expect(blocked_account.remaining_failed_attempts).to eq 0
  end
end
