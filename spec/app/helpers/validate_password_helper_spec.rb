require 'spec_helper'

describe ValidatePasswordHelper do
  class ValidatePasswordHelperClass
    include ValidatePasswordHelper
  end

  let(:helper) { ValidatePasswordHelperClass.new }

  it 'valid password' do
    expect(helper.valid_password('AAAasd123!!')).not_to be_nil
  end

  it 'short password is not valid' do
    expect(helper.valid_password('asd')).to be_nil
  end

  it 'long password is not valid' do
    expect(helper.valid_password('A!a91890928162736271928')).to be_nil
  end

  it 'password without uppercase letter is not valid' do
    expect(helper.valid_password('a!12377871')).to be_nil
  end

  it 'password without lowercase letter is not valid' do
    expect(helper.valid_password('!AAA121123')).to be_nil
  end

  it 'password without symbol is not valid' do
    expect(helper.valid_password('1a1A123456')).to be_nil
  end

  it 'password without number is not valid' do
    expect(helper.valid_password('aaaaAAAA!!')).to be_nil
  end
end
