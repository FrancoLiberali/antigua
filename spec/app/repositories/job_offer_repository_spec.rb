require 'integration_spec_helper'

describe JobOfferRepository do
  let(:repository) { described_class.new }

  let(:owner) do
    user = User.new(name: 'Joe', email: 'joe@doe.com', crypted_password: 'secure_pwd')
    UserRepository.new.save(user)
    user
  end

  describe 'deactive_old_offers' do
    let!(:today_offer) do
      today_offer = JobOffer.new(title: 'a title',
                                 updated_on: Date.today,
                                 is_active: true,
                                 user_id: owner.id)
      repository.save(today_offer)
      today_offer
    end

    let!(:thirty_day_offer) do
      thirty_day_offer = JobOffer.new(title: 'a title',
                                      updated_on: Date.today - 45,
                                      is_active: true,
                                      user_id: owner.id)
      repository.save(thirty_day_offer)
      thirty_day_offer
    end

    it 'should deactivate offers updated 45 days ago' do
      repository.deactivate_old_offers

      updated_offer = repository.find(thirty_day_offer.id)
      expect(updated_offer.is_active).to eq false
    end

    it 'should not deactivate offers created today' do
      repository.deactivate_old_offers

      not_updated_offer = repository.find(today_offer.id)
      expect(not_updated_offer.is_active).to eq true
    end
  end

  describe 'search offers' do
    before(:each) do
      offer1 = JobOffer.new(title: 'Java dev',
                            description: 'full time',
                            location: 'BsAs',
                            is_active: true,
                            user_id: owner.id)
      offer2 = JobOffer.new(title: 'Developer',
                            description: 'ruby part time',
                            location: 'BSAS',
                            is_active: true,
                            user_id: owner.id)
      offer3 = JobOffer.new(title: 'Ruby DEV',
                            description: 'full time',
                            location: 'CABA',
                            is_active: true,
                            user_id: owner.id)
      offer4 = JobOffer.new(title: 'Tester',
                            description: 'Tester full time',
                            location: 'BSAS',
                            is_active: false,
                            user_id: owner.id)
      repository.save(offer1)
      repository.save(offer2)
      repository.save(offer3)
      repository.save(offer4)
    end

    it 'search by title case INsensitive' do
      expect(repository.search_offer('dev').size).to eq 3
    end

    it 'search on title description' do
      expect(repository.search_offer('ruby').size).to eq 2
    end

    it 'search by location' do
      expect(repository.search_offer('BsAs').size).to eq 2
    end

    it 'search only activate offers' do
      expect(repository.search_offer('BSAS').size).to eq 2
    end
  end
end
