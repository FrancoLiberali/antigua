class GateKeeper
  def initialize
    @auth_succeed = false
  end

  def authenticate(email, password)
    @user = UserRepository.new.find_by_email(email)
    if @user
      @auth_succeed = @user.has_password?(password)
      UserRepository.new.save(@user)
    end
    self
  end

  def when_succeed
    yield(@user) if @auth_succeed
    self
  end

  def when_failed
    yield(@user) unless @auth_succeed
    self
  end
end
