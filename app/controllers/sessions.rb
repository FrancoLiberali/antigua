JobVacancy::App.controllers :sessions do
  get :login, map: '/login' do
    @user = User.new
    render 'sessions/new'
  end

  post :create do
    email = params[:user][:email]
    password = params[:user][:password]

    gate_keeper = GateKeeper.new.authenticate(email, password)

    gate_keeper.when_succeed do |user|
      @user = user
      sign_in @user
      redirect '/'
    end

    gate_keeper.when_failed do |user|
      error_message = 'Invalid credentials'
      if user
        if user.blocked?
          error_message =
            "Your account has been blocked for
#{Padrino.config.blocking_account_time_message}"
        else
          error_message += ", you have #{user.remaining_failed_attempts} more failed log
in attempts before your account get blocked for
#{Padrino.config.blocking_account_time_message}"
        end
      end
      flash[:error] = error_message
      redirect '/login'
    end
  end

  get :destroy, map: '/logout' do
    sign_out
    redirect '/'
  end
end
