JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  post :create do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }

    @user = User.new(params[:user])

    if !valid_password params[:user][:password]
      flash.now[:error] = 'The password must be 8-20 characters long,
      contain at least one number, one symbol and have a mixture of uppercase
      and lowercase letters.'
      render 'users/new'
    elsif params[:user][:password] != password_confirmation
      flash.now[:error] = 'Passwords do not match'
      render 'users/new'
    elsif UserRepository.new.save(@user)
      flash[:success] = 'User created'
      redirect '/'
    else
      flash.now[:error] = 'All fields are mandatory'
      render 'users/new'
    end
  end
end
