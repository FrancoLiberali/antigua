require_relative '../../models/blocked_account'
require_relative '../../models/active_account'

class UserRepository < BaseRepository
  self.table_name = :users
  self.model_class = 'User'

  def find_by_email(email)
    row = dataset.first(email: email)
    load_object(row) unless row.nil?
  end

  def save(user)
    return super(user) if user.account_state.nil?

    if user.account_state.class == ActiveAccount
      ActiveAccountRepository.new.save(user.account_state)
    else
      BlockedAccountRepository.new.save(user.account_state)
    end
    user.account_state_id = user.account_state&.id
    super(user)
  end

  protected

  def changeset(user)
    {
      name: user.name,
      crypted_password: user.crypted_password,
      email: user.email,
      account_state_id: user.account_state_id
    }
  end

  def load_object(a_record)
    user = super(a_record)
    user.account_state = BlockedAccountRepository.new.find_nullable(user.account_state_id) ||
                         ActiveAccountRepository.new.find_nullable(user.account_state_id)
    user
  end
end
