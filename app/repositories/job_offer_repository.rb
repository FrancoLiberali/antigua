class JobOfferRepository < BaseRepository
  self.table_name = :job_offers
  self.model_class = 'JobOffer'

  def all_active
    load_collection dataset.where(is_active: true)
  end

  def find_by_owner(user)
    load_collection dataset.where(user_id: user.id)
  end

  def deactivate_old_offers
    all_active.each do |offer|
      if offer.old_offer?
        offer.deactivate
        update(offer)
      end
    end
  end

  def search_offer(key_word)
    search_on = %i[title description location]
    load_collection dataset.where(is_active: true).grep(search_on, "%#{key_word}%",
                                                        case_insensitive: true).all
  end

  protected

  def load_object(a_record)
    job_offer = super
    # TODO: Eager load user to avoid N+1 queries
    user = UserRepository.new.find(job_offer.user_id)
    job_offer.owner = user
    job_offer
  end

  def changeset(offer)
    {
      title: offer.title,
      location: offer.location,
      description: offer.description,
      is_active: offer.is_active,
      user_id: offer.owner&.id || offer.user_id,
      applications_amount: offer.applications_amount,
      validity_date: offer.validity_date == '' ? nil : offer.validity_date,
      tags: offer.tags
    }
  end
end
