class ActiveAccountRepository < BaseRepository
  self.table_name = :active_accounts
  self.model_class = 'ActiveAccount'

  protected

  def changeset(active_account)
    {
      remaining_failed_attempts: active_account.remaining_failed_attempts
    }
  end
end
