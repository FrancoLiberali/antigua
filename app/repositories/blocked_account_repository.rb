class BlockedAccountRepository < BaseRepository
  self.table_name = :blocked_accounts
  self.model_class = 'BlockedAccount'

  protected

  def changeset(blocked_account)
    {
      blocked_on: blocked_account.blocked_on
    }
  end
end
