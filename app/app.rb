module JobVacancy
  class App < Padrino::Application
    register Padrino::Rendering
    register Padrino::Mailer
    register Padrino::Helpers

    enable :sessions

    Padrino.configure :development, :test do
      set :delivery_method, file: {
        location: "#{Padrino.root}/tmp/emails"
      }
      Padrino.config.blocking_account_time = 10
      Padrino.config.blocking_account_time_message = '10 seconds'
    end

    Padrino.configure :staging, :production do
      set :delivery_method, smtp: {
        address: ENV['SMTP_ADDRESS'],
        port: ENV['SMTP_PORT'],
        user_name: ENV['SMTP_USER'],
        password: ENV['SMTP_PASS'],
        authentication: :plain,
        enable_starttls_auto: true
      }
    end

    Padrino.configure :staging do
      Padrino.config.blocking_account_time = 1 * 60
      Padrino.config.blocking_account_time_message = '1 minute'
    end

    Padrino.configure :production do
      Padrino.config.blocking_account_time = 12 * 60 * 60
      Padrino.config.blocking_account_time_message = '12 hours'
    end
  end
end
